/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tijo.pkg04;

/**
 *
 * @author student
 */
public class Main {
    static long LIMIT=25;
    private long getFactorial(final long digit) throws Main.DigitTooLow, Main.DigitTooHigh {
        if(digit<1) throw new Main.DigitTooLow();
        else if(digit>LIMIT) throw new Main.DigitTooHigh();
        else if(digit==1) return digit;
        else return digit*getFactorial(digit-1);
    }
    class DigitTooLow extends Exception {}
    class DigitTooHigh extends Exception {}
    public static void main(String[] args) {
        Main main=new Main();
        long digit = 25;
        try{
        long factorial = main.getFactorial(digit);
        System.out.printf("Silnia(%d) = %d \n", digit, factorial);
        } catch (Main.DigitTooLow ex){
            System.err.println("Liczba zbyt mała");
        } catch (DigitTooHigh ex) {
            System.err.println("Liczba zbyt duża");
        }
        
    }
}
